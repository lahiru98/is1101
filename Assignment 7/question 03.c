#include<stdio.h>
#include<string.h>
int main()
{
    int check=0,r1=0,c1=0,r2=0,c2=0;
    printf("Enter the number of rows & columns for the First Matrix:\n");
    scanf("%d \n%d",&r1, &c1);
    printf("\nEnter the number of rows & columns for the Second Matrix:\n");
    scanf("%d \n%d",&r2, c2);

    int matrix1[r1][c1],matrix2[r2][c2];
    printf("Enter elements of First Matrix:\n");
    for(int i=0; i<r1; i++)
        {
        for(int j=0; j<c1; j++)
            {
            printf("Enter the data of %d th row & %d th column:",i+1,j+1);
            scanf("%d",&matrix1[i][j]);
            }
        }
    printf("First Matrix:\n");
    for(int i=0; i<r1; i++)
        {
        printf("|");
        for(int j=0; j<c1; j++)
            {
            printf("%4d",matrix1[i][j]);
            }
        printf("%4c|\n",'\0');
        }
    printf("\nEnter elements of Second Matrix:\n");
    for(int i=0; i<r2; i++)
        {
        for(int j=0; j<c2; j++)
            {
            printf("Enter the data of %d th row & %d th column:",i+1,j+1);
            scanf("%d",&matrix2[i][j]);
            }
        }
    printf("Second Matrix:\n");
    for(int i=0; i<r2; i++)
        {
        printf("|");
        for(int j=0; j<c2; j++)
            {
            printf("%4d",matrix2[i][j]);
            }
        printf("%4c|\n",'\0');
        }

    if(r1==r2 && c1==c2)
        {
        int output[r1][c1];
        for(int i=0; i<r1; i++)
            {
            for(int j=0; j<c1; j++)
                {
                output[i][j]=matrix1[i][j]+matrix2[i][j];
                }
            }
        printf("\nFirst Matrix + Second Matrix :\n");
        for(int i=0; i<r2; i++)
            {
            printf("|");
            for(int j=0; j<c2; j++)
                {
                printf("%4d",output[i][j]);
                }
            printf("%4c|\n",'\0');
            }
        }
    else
        {
        printf("Can't add First Matrix and Second Matrix.\n");
        }
    if(c1==r2)
        {
        int output2[r1][c2];
        for(int i=0; i<r1; i++)
            {
            for(int j=0; j<c2; j++)
                {
                output2[i][j]=0;
                }
            }
        for (int i = 0; i < r1; i++)
            {
            for (int j = 0; j < c2; j++)
                {
                for (int k = 0; k < c1; k++)
                    {
                    output2[i][j] += matrix1[i][k] * matrix2[k][j];
                    }
                }
            }
        printf("\nFirst Matrix * Second Matrix :\n");
        for(int i=0; i<r1; i++)
            {
            printf("|");
            for(int j=0; j<c2; j++)
                {
                printf("%4d",output2[i][j]);
                }
            printf("%4c|\n",'\0');
            }
        }
    else
        {
        printf("Can't multiply First Matrix & Second Matrix.");
        }
    return 0;
}
